## 1. Cel projektu:

zautomatyzowanie testów API: https://jsonplaceholder.typicode.com/


## 2. Założenia projektu:

- [ ] uruchamianie testów wielowątkowo
- [ ] uruchamianie testów w ramach ciągłej integracji
- [x] umożliwość parametryzacji testów
- [x] zgodność testów z FIRST(Fast, Independent, Repeatable, Self-Validating, Timely.
- [x] tworzenie raportów z wykonania testów (Allure)
- [x] implementacja Request Object Pattern
- [x] sprzątanie po testach
- [ ] powtarzanie testów w przypadku porażki

## 3. Pokryte scenariusze testowe:



## 4. Wykorzystane freamworki:

   - REST Assured - narzędzie pozwalające na automatyzację testów API 
   - TestNG - biblioteka programistyczna służąca do pisania testów
   - Apache Maven - narzędzie automatyzujące budowę oprogramowania
   - AssertJ - biblioteka dostarczająca bogaty zestaw asercji
   - JavaFaker - biblioteka dostarczająca losowych danych
   - Google Gson - biblioteka służąca do serializacji i deserializacji obiektów Java do formatu JSON  
   - Aeonbits Owner - biblioteka ułatwiająca odczyt danych z plików _properties_
   - Allure - freamework pozwalający tworzyć raporty z testów automatycznych



## 5. Opis pakietów i klas:

   


## 6. Co dalej?

   
    
