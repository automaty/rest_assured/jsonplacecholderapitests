package pl.automaty.restassured.test.post;

import org.testng.annotations.Test;
import pl.automaty.restassured.main.pojo.post.Post;
import pl.automaty.restassured.test.testbases.SuiteTestBase;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

public class CreatePostTests extends SuiteTestBase {

    @Test
    public void givenPostWhenPostPostThenPostIsCreatedTest() {

        Post post = new Post();
        post.setId(101);
        post.setUserId(10);
        post.setTitle("My post");
        post.setBody("This is my first post.");

        Post postResponse = given().contentType("application/json").body(post)
                .when().post("posts")
                .then().statusCode(201)
                .extract().as(Post.class);

        assertEquals(postResponse.getId(), post.getId(), "Id");
        assertEquals(postResponse.getUserId(), post.getUserId(), "UserId");
        assertEquals(postResponse.getBody(), post.getBody(), "Body");
        assertEquals(postResponse.getTitle(), post.getTitle(), "Title");
    }

}
