package pl.automaty.restassured.test.testbases;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeSuite;
import pl.automaty.restassured.main.properties.EnvConfig;

public class SuiteTestBase {

    @BeforeSuite
    public void beforeSuite() {
        EnvConfig envConfig = ConfigFactory.create(EnvConfig.class);

        RestAssured.baseURI = envConfig.baseUri();
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
        RestAssured.useRelaxedHTTPSValidation();
    }
}
