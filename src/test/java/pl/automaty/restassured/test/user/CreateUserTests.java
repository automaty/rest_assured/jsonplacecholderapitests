package pl.automaty.restassured.test.user;

import org.testng.annotations.Test;
import pl.automaty.restassured.main.pojo.user.Address;
import pl.automaty.restassured.main.pojo.user.Company;
import pl.automaty.restassured.main.pojo.user.Geo;
import pl.automaty.restassured.main.pojo.user.User;
import pl.automaty.restassured.test.testbases.SuiteTestBase;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.Assert.assertEquals;

public class CreateUserTests  extends SuiteTestBase {

    @Test
    public void givenUserWhenPostUserThenUserIsCreatedTest() {

        Company company = new Company("Sferis.net", "Multiplayer realtime games", "Multiplayer games");
        Geo geo = new Geo("-37.83523", "144.97806");
        Address address = new Address("Bromby", "9", "Melbourne", "VIC 3141", geo);
        User user = User.Builder.create()
                .withId(11)
                .withUsername("Marky")
                .withName("Mark")
                .withEmail("marky91@gmail.com")
                .withPhone("123123123")
                .withWebsite("sferis.net")
                .withAddress(address)
                .withCompany(company)
                .build();

        User actualUser = given().body(user).contentType("application/json")
                .when().post("users")
                .then().statusCode(201)
                .extract().as(User.class);

        assertEquals(actualUser.getUsername(), user.getUsername(), "Username");
        assertEquals(actualUser.getId(), user.getId(), "User id");
        assertEquals(actualUser.getCompany().getName(), user.getCompany().getName(), "User Company name");
    }
}
